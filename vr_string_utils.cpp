#include <iostream>
#include <locale>   //std::locale, std::toupper, std::tolower

#include "vr_string_utils.h"

namespace libutils
{

bool isAlpha(const char& src)
{
    std::locale loc;

    return std::isalpha(src, loc);
}

void toUpper(string& src)
{
    std::locale loc;
    
    for (string::size_type i = 0; i < src.length(); ++i)
    {
        src[i] = std::toupper(src[i], loc);    
    }
}

void toLower(string& src)
{
    std::locale loc;
    
    for (string::size_type i = 0; i < src.length(); ++i)
    {
        src[i] = std::tolower(src[i], loc);    
    } 
}

void swapUpperLower(string& src)
{
    std::locale loc;
    
    for (string::size_type i = 0; i < src.length(); ++i)
    {
        if (std::isupper(src[i], loc))
        {
            src[i] = std::tolower(src[i], loc); 
        }
        else if (std::islower(src[i], loc))
        {
            src[i] = std::toupper(src[i], loc);
        }
    }
}

}
