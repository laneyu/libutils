#include <string.h>

#include "vr_cipher_utils.h"
#include "vr_string_utils.h"

namespace libutils
{

const char* g_ROT13CodeStr = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
const char* g_ROT13DecodeStr = "NOPQRSTUVWXYZABCDEFGHIJKLMnopqrstuvwxyzabcdefghijklm";

void ROT13Code(string& src)
{
    const char* pch;
    
    for (string::size_type i = 0; i < src.length(); ++i)
    {
        if (isAlpha(src[i]) == true)
        {
            pch = strchr(g_ROT13CodeStr, src[i]);
            if (pch != NULL)
            {
                src[i] = g_ROT13DecodeStr[pch-g_ROT13CodeStr];
            }
            
        }
    }
}

void ROT13Decode(string& src)
{
    const char* pch;
        
    for (string::size_type i = 0; i < src.length(); ++i)
    {
        if (isAlpha(src[i]) == true)
        {
            pch = strchr(g_ROT13DecodeStr, src[i]);
            if (pch != NULL)
            {
                src[i] = g_ROT13CodeStr[pch-g_ROT13DecodeStr];
            }
                
        }
    }
}
    
}
