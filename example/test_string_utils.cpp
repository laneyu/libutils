#include <iostream>

#include "../vr_string_utils.h"

using namespace libutils;

int main()
{
    string src = "Hello World";
    std::cout <<src+"\n";
    swapUpperLower( src);
    std::cout <<src+"\n";
    toUpper( src);
    std::cout <<src+"\n";
    toLower(src);
    std::cout <<src+"\n";
}
