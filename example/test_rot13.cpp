#include <iostream>

#include "../vr_cipher_utils.h"

using namespace libutils;

int main()
{
    string code = "ABCDabcd";
    ROT13Code(code);
    std::cout << code <<"\n";

    ROT13Decode(code);
    std::cout << code <<"\n";
}
