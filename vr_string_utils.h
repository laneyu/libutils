#ifndef __VR_STRING_UTILS_H__
#define __VR_STRING_UTILS_H__

#ifdef	LIBUTILS_STD_STRING
#include <string>
#else
#include <ext/vstring.h>
#include <ext/vstring_fwd.h>
#endif

namespace libutils
{

#ifdef	LIBUTILS_STD_STRING
using string;
#else
typedef __gnu_cxx::__sso_string string;
#endif

// true if indeed c is a alphabetic charactor
// false otherwise
bool isAlpha(const char& src);
void toUpper(string& src);
void toLower(string& src);
void swapUpperLower(string& src);

}
#endif