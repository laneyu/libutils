#ifndef __VR_CIPHER_UTILS_H__
#define __VR_CIPHER_UTILS_H__

#include "vr_string_utils.h"

namespace libutils
{
void ROT13Code(string& src);
void ROT13Decode(string& src);
}

#endif